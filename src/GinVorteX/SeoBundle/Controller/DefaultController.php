<?php

namespace GinVorteX\SeoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GinVorteX\SeoBundle\Manager\SEOManager;

class DefaultController extends BaseController {

    /**
     * @Route("/", name="SEOBundles_index")
     * @Template("GinVorteXSeoBundle::index.html.twig")
     */
    public function indexAction() {
        return array('name' => 'Tommy');
    }

    /**
     * @Route("/testing", name="seo_testing")
     * @Template("GinVorteXSeoBundle::index.html.twig")
     */
    public function seoTestingAction(\Symfony\Component\HttpFoundation\Request $request) {
        $seo = new SEOManager($request);
        
        return array(
            'name' => 'Tommy',
            'seo' => $seo->render(),
        );
    }

}
