<?php

namespace GinVorteX\SeoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GinVorteX\SeoBundle\Entity\MetaWeb;
use GinVorteX\SeoBundle\Form\MetaWebType;

/**
 * MetaWeb controller.
 *
 * @Route("/metaweb")
 */
class MetaWebController extends BaseController {

    /**
     * Lists all MetaWeb entities.
     *
     * @Route("/", name="SEOBundles_meta")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->init('GinVorteXSeoBundle:MetaWeb');

        $entities = $this->repo->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new MetaWeb entity.
     *
     * @Route("/", name="SEOBundles_meta_create")
     * @Method("POST")
     */
    public function createAction(Request $request) {
        try {
            $this->init('GinVorteXSeoBundle:MetaWeb');
            
            $errorMessage = null;
            $post = $request->request->all();
            $repo = $this->repo;

            if (!array_key_exists('path', $post)) {
                $errorMessage = 'Please Insert Path';
            }

            if ($errorMessage)
                return new JsonResponse(array(
                    'errorMessage' => $errorMessage,
                        ), JsonResponse::HTTP_BAD_REQUEST);

            $entity = new MetaWeb();
            $entity->setPath($post['path'])
                    ->setTitle($post['title'])
                    ->setDescription($post['description'])
                    ->setKeywords($post['keywords'])
                    ->setImagePage($post['imagePage'])
                    ->setAltImage($post['altImage'])
                    ->setAltTitle($post['altTitle'])
                    ->setCloudText($post['cloudText'])
                    ->setCloudKeyword($post['cloudKeyword']);

            $this->em->persist($entity);
            $this->em->flush();

            return new JsonResponse(array(
                'entity' => $repo->getEntityData($entity),
                    ), JsonResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            return new JsonResponse(array(
                'errorMessage' => $e->getMessage(),
                    ), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Creates a form to create a MetaWeb entity.
     *
     * @param MetaWeb $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MetaWeb $entity) {
        $form = $this->createForm(new MetaWebType(), $entity, array(
            'action' => $this->generateUrl('SEOBundles_meta_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MetaWeb entity.
     *
     * @Route("/new", name="SEOBundles_meta_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new MetaWeb();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a MetaWeb entity.
     *
     * @Route("/{id}", name="SEOBundles_meta_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->init('GinVorteXSeoBundle:MetaWeb');

        $entity = $this->repo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MetaWeb entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing MetaWeb entity.
     *
     * @Route("/{id}/edit", name="SEOBundles_meta_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->init('GinVorteXSeoBundle:MetaWeb');

        $entity = $this->repo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MetaWeb entity.');
        }

        $entity->setCloudKeyword(json_decode($entity->getCloudKeyword(), true));
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'keywords' => $entity->getCloudKeyword(),
        );
    }

    /**
     * Creates a form to edit a MetaWeb entity.
     *
     * @param MetaWeb $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(MetaWeb $entity) {
        $form = $this->createForm(new MetaWebType(), $entity, array(
            'action' => $this->generateUrl('SEOBundles_meta_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing MetaWeb entity.
     *
     * @Route("/{id}", name="SEOBundles_meta_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id) {
        try {
            $this->init('GinVorteXSeoBundle:MetaWeb');
            
            $errorMessage = null;
            $post = $request->request->all();
            $repo = $this->repo;

            $entity = $this->repo->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MetaWeb entity.');
            }

            if (array_key_exists('path', $post) && !empty($post['path']))
                $entity->setPath($post['path']);
            if (array_key_exists('title', $post) && !empty($post['title']))
                $entity->setTitle($post['title']);
            if (array_key_exists('description', $post) && !empty($post['description']))
                $entity->setDescription($post['description']);
            if (array_key_exists('keywords', $post) && !empty($post['keywords']))
                $entity->setKeywords($post['keywords']);
            if (array_key_exists('imagePage', $post) && !empty($post['imagePage']))
                $entity->setImagePage($post['imagePage']);
            if (array_key_exists('altImage', $post) && !empty($post['altImage']))
                $entity->setAltImage($post['altImage']);
            if (array_key_exists('altTitle', $post) && !empty($post['altTitle']))
                $entity->setAltTitle($post['altTitle']);
            if (array_key_exists('cloudText', $post) && !empty($post['cloudText']))
                $entity->setCloudText($post['cloudText']);
            if (array_key_exists('cloudKeyword', $post) && !empty($post['cloudKeyword']))
                $entity->setCloudKeyword($post['cloudKeyword']);

            $this->em->persist($entity);
            $this->em->flush();

            return new JsonResponse(array(
                'entity' => $repo->getEntityData($entity),
                    ), JsonResponse::HTTP_ACCEPTED);
        } catch (\Exception $e) {
            return new JsonResponse(array(
                'errorMessage' => $e->getMessage(),
                    ), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deletes a MetaWeb entity.
     *
     * @Route("/{id}", name="SEOBundles_meta_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->init('GinVorteXSeoBundle:MetaWeb');
        
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity = $this->repo->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MetaWeb entity.');
            }

            $this->em->remove($entity);
            $this->em->flush();
        }

        return $this->redirect($this->generateUrl('SEOBundles_meta'));
    }

    /**
     * Creates a form to delete a MetaWeb entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('SEOBundles_meta_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
