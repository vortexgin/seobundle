<?php

namespace GinVorteX\SeoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GinVorteX\SeoBundle\Entity\GlobalConfig;
use GinVorteX\SeoBundle\Form\GlobalConfigType;

/**
 * GlobalConfig controller.
 *
 * @Route("/globalconfig")
 */
class GlobalConfigController extends BaseController {

    /**
     * Lists all GlobalConfig entities.
     *
     * @Route("/", name="SEOBundles_config")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->init('GinVorteXSeoBundle:GlobalConfig');
        
        $entities = $this->repo->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new GlobalConfig entity.
     *
     * @Route("/", name="SEOBundles_config_create")
     * @Method("POST")
     * @Template("GinVorteXSeoBundle:GlobalConfig:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->init('GinVorteXSeoBundle:GlobalConfig');
        
        $entity = new GlobalConfig();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($entity);
            $this->em->flush();

            return $this->redirect($this->generateUrl('SEOBundles_config_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a GlobalConfig entity.
     *
     * @param GlobalConfig $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(GlobalConfig $entity) {
        $form = $this->createForm(new GlobalConfigType(), $entity, array(
            'action' => $this->generateUrl('SEOBundles_config_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new GlobalConfig entity.
     *
     * @Route("/new", name="SEOBundles_config_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new GlobalConfig();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a GlobalConfig entity.
     *
     * @Route("/{id}", name="SEOBundles_config_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->init('GinVorteXSeoBundle:GlobalConfig');

        $entity = $this->repo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GlobalConfig entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing GlobalConfig entity.
     *
     * @Route("/{id}/edit", name="SEOBundles_config_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->init('GinVorteXSeoBundle:GlobalConfig');

        $entity = $this->repo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GlobalConfig entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a GlobalConfig entity.
     *
     * @param GlobalConfig $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(GlobalConfig $entity) {
        $form = $this->createForm(new GlobalConfigType(), $entity, array(
            'action' => $this->generateUrl('SEOBundles_config_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing GlobalConfig entity.
     *
     * @Route("/{id}", name="SEOBundles_config_update")
     * @Method("PUT")
     * @Template("GinVorteXSeoBundle:GlobalConfig:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $this->init('GinVorteXSeoBundle:GlobalConfig');

        $entity = $this->repo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GlobalConfig entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->em->flush();

            return $this->redirect($this->generateUrl('SEOBundles_config_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a GlobalConfig entity.
     *
     * @Route("/{id}", name="SEOBundles_config_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->init('GinVorteXSeoBundle:GlobalConfig');
        
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity = $this->repo->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GlobalConfig entity.');
            }

            $this->em->remove($entity);
            $this->em->flush();
        }

        return $this->redirect($this->generateUrl('SEOBundles_config'));
    }

    /**
     * Creates a form to delete a GlobalConfig entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('SEOBundles_config_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
