<?php

namespace GinVorteX\SeoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GinVorteX\SeoBundle\Entity\TargetKeyword;
use GinVorteX\SeoBundle\Form\TargetKeywordType;

/**
 * TargetKeyword controller.
 *
 * @Route("/targetkeyword")
 */
class TargetKeywordController extends BaseController {
    /**
     * Lists all TargetKeyword entities.
     *
     * @Route("/", name="SEOBundles_keyword")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->init('GinVorteXSeoBundle:TargetKeyword');

        $entities = $this->repo->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new TargetKeyword entity.
     *
     * @Route("/", name="SEOBundles_keyword_create")
     * @Method("POST")
     * @Template("GinVorteXSeoBundle:TargetKeyword:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->init('GinVorteXSeoBundle:TargetKeyword');
        
        $entity = new TargetKeyword();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($entity);
            $this->em->flush();

            return $this->redirect($this->generateUrl('SEOBundles_keyword_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a TargetKeyword entity.
     *
     * @param TargetKeyword $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TargetKeyword $entity) {
        $form = $this->createForm(new TargetKeywordType(), $entity, array(
            'action' => $this->generateUrl('SEOBundles_keyword_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TargetKeyword entity.
     *
     * @Route("/new", name="SEOBundles_keyword_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new TargetKeyword();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a TargetKeyword entity.
     *
     * @Route("/{id}", name="SEOBundles_keyword_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->init('GinVorteXSeoBundle:TargetKeyword');

        $entity = $this->repo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TargetKeyword entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing TargetKeyword entity.
     *
     * @Route("/{id}/edit", name="SEOBundles_keyword_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->init('GinVorteXSeoBundle:TargetKeyword');

        $entity = $this->repo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TargetKeyword entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a TargetKeyword entity.
     *
     * @param TargetKeyword $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(TargetKeyword $entity) {
        $form = $this->createForm(new TargetKeywordType(), $entity, array(
            'action' => $this->generateUrl('SEOBundles_keyword_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing TargetKeyword entity.
     *
     * @Route("/{id}", name="SEOBundles_keyword_update")
     * @Method("PUT")
     * @Template("GinVorteXSeoBundle:TargetKeyword:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $this->init('GinVorteXSeoBundle:TargetKeyword');

        $entity = $this->repo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TargetKeyword entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->em->flush();

            return $this->redirect($this->generateUrl('SEOBundles_keyword_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a TargetKeyword entity.
     *
     * @Route("/{id}", name="SEOBundles_keyword_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->init('GinVorteXSeoBundle:TargetKeyword');
        
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity = $this->repo->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TargetKeyword entity.');
            }

            $this->em->remove($entity);
            $this->em->flush();
        }

        return $this->redirect($this->generateUrl('SEOBundles_keyword'));
    }

    /**
     * Creates a form to delete a TargetKeyword entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('SEOBundles_keyword_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
