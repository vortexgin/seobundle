<?php

namespace GinVorteX\SeoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends Controller {
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    protected $em;

    /**
     * @var \DateTime $timeInit
     */
    protected $timeInit;
    
    protected $repo;
    
    protected function init($entityName) {
        date_default_timezone_set('Asia/Jakarta');
        $this->timeInit = new \DateTime;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->repo = $this->em->getRepository($entityName);
    }
    
    protected function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}