<?php

namespace GinVorteX\SeoBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * BaseRepository
 *
 */
class BaseRepository extends EntityRepository {
    const ORDER_ASC = 'ASC';
    const ORDER_DESC = 'DESC';
    public static $LIST_ORDER = array(self::ORDER_ASC, self::ORDER_DESC);

    const LIMIT = 20;

    protected function cleanFetchAllParameter(array $filter, array $availableOrderBy = []) {
        $requiredOrderBy = [
            'id',
            'postingDate',
            'updateDate',
        ];
        $orderBy = array_merge($requiredOrderBy, $availableOrderBy);

        if (isset($filter['order_by']) && !in_array($filter['order_by'], $orderBy))
            unset($filter['order_by']);
        if (isset($filter['order_type']) && !in_array($filter['order_type'], array('ASC', 'DESC')))
            unset($filter['order_type']);

        return $filter;
    }

    protected function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
