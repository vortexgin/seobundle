<?php

namespace GinVorteX\SeoBundle\Repository;

use GinVorteX\SeoBundle\Repository\BaseRepository;
use GinVorteX\SeoBundle\Entity\GlobalConfig;

/**
 * GlobalConfigRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GlobalConfigRepository extends BaseRepository {
    /**
    * Function to extract entity data.
    *
    * @param \ORORI\CoreBundle\Entity\GlobalConfig $obj
    *
    * @return array
    */
    public function getEntityData(GlobalConfig $obj){
        return array(
            'id'            => $obj->getId(), 
            'domain'        => $obj->getDomain(), 
            'facebook'      => $obj->getFacebook(), 
            'twitter'       => $obj->getTwitter(), 
            'gplus'         => $obj->getGplus(), 
            'defaultImage'  => $obj->getDefaultImage(), 
            'language'      => $obj->getLanguage(), 
            'country'       => $obj->getCountry(), 
            'geoLocation'   => $obj->getGeoLocation(), 
            'posting_date'  => $obj->getPostingDate(), 
            'update_date'   => $obj->getUpdateDate(), 
        );
    }
    
}
