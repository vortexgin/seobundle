<?php

namespace GinVorteX\SeoBundle\Manager;

use GinVorteX\SeoBundle\Manager\BaseManager;
use GinVorteX\SeoBundle\Entity\GlobalConfig;

class GlobalConfigManager extends BaseManager{
    private $repo;
    
    public function __construct() {
        parent::__construct();
        
        $this->repo = $this->_em->getRepository('GinVorteXSeoBundle:GlobalConfig');
    }
    
    public function getAll(array $filter = array(), array $sort = array('id' => 'DESC')){
        $data = $this->repo->findBy($filter, $sort);
        
        if(!$data)
            return false;
        
        foreach($data as $key=>$obj){
            $return[] = $this->repo->getEntityData($obj);
        }
        
        return $return;
    }
    
    public function getId($id){
        $data = $this->repo->findOneById($id);
        
        if(!$data)
            return false;
        
        return $this->repo->getEntityData($data);
    }
}