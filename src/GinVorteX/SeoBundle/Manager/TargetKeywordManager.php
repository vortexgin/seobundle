<?php

namespace GinVorteX\SeoBundle\Manager;

use GinVorteX\SeoBundle\Manager\BaseManager;
use GinVorteX\SeoBundle\Entity\TargetKeyword;

class TargetKeywordManager extends BaseManager{
    private $repo;
    
    public function __construct() {
        parent::__construct();
        
        $this->repo = $this->_em->getRepository('GinVorteXSeoBundle:TargetKeyword');
    }
    
    public function getAll(array $filter = array(), array $sort = array('id' => 'DESC')){
        $data = $this->repo->findBy($filter, $sort);
        
        if(!$data)
            return false;
        
        foreach($data as $key=>$obj){
            $return[] = $this->repo->getEntityData($obj);
        }
        
        return $return;
    }
    
    public function getId(int $id){
        $data = $this->repo->findOneById($id);
        
        if(!$data)
            return false;
        
        return $this->repo->getEntityData($data);
    }
}