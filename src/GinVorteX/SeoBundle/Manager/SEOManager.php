<?php

namespace GinVorteX\SeoBundle\Manager;

use Symfony\Component\HttpFoundation\Request;

use GinVorteX\SeoBundle\Manager\GlobalConfigManager;
use GinVorteX\SeoBundle\Manager\TargetKeywordManager;
use GinVorteX\SeoBundle\Manager\MetaWebManager;

use GinVorteX\SeoBundle\Entity\GlobalConfig;
use GinVorteX\SeoBundle\Entity\MetaWeb;

class SEOManager extends BaseManager{
    private $route;
    
    private $managerGlobalConfig;
    private $managerMetaWeb;
    
    private $objGlobalConfig;
    private $objTargetKeyword;
    private $objMetaWeb;
    
    
    public function __construct(Request $request) {
        parent::__construct();
        
        $this->route = $request->get('_route');
        $this->managerGlobalConfig  = new GlobalConfigManager();
        $this->managerMetaWeb       = new MetaWebManager();
    }
    
    public function render(){
        $this->findGlobalConfig();
        $this->findMetaWeb();
        
        return array(
            'meta'          => $this->renderMeta(), 
            'cloudText'     => $this->renderCloudText(), 
            'cloudKeyword'  => $this->renderCloudKeyword(), 
        );
    }
    
    private function findGlobalConfig(){
        $config = $this->managerGlobalConfig->getId(1);
        if(!$config){
            $repoConfig  = $this->_em->getRepository('GinVorteXSeoBundle:GlobalConfig');
            $new = new GlobalConfig();
            $config = $repoConfig->getEntityData($new);
        }
        $this->objGlobalConfig = $config;        
    }
    
    private function findMetaWeb(){
        $meta = $this->managerMetaWeb->getAll(array(
            'path' => $this->route
        ));
        if(!$meta){
            $repoMeta  = $this->_em->getRepository('GinVorteXSeoBundle:MetaWeb');
            $new = new MetaWeb();
            $meta = $repoMeta->getEntityData($new);            
        }
        if(is_array($meta)){
            if(array_key_exists('id', $meta)){
                $this->objTargetKeyword = $meta['cloudKeyword'];
                $meta = array($meta);
            }else{                
                $this->objTargetKeyword = $meta[0]['cloudKeyword'];
            }
        }
        $this->objMetaWeb = $meta;
    }
    
    private function renderMeta(){
        return $this->renderView(
            'GinVorteXSeoBundle:Render:meta.html.twig',
            array(
                'config'    => $this->objGlobalConfig, 
                'meta'      => $this->objMetaWeb[0], 
            )
        );
    }
    
    private function renderCloudText(){
        $text = $this->objMetaWeb[0]['cloudText'];
        $keyword = $this->objMetaWeb[0]['keywords'];
        if(!empty($keyword)){
            $keywords = explode(',' , $keyword);
            if(count($keywords) <= 1)
                $keywords = array($keywords);
            
            foreach($keywords as $key=>$value){
                $replace[] = '<strong>'.$value.'</strong>';
            }
            
            $text = str_replace($keywords, $replace, $text);
        }
        
        return $text;
    }
    
    private function renderCloudKeyword(){
        $return = null;
        if(is_array($this->objTargetKeyword)){
            $keywords = array();
            foreach($this->objTargetKeyword as $key=>$keyword){
                $keywords[] = '<a href="'.$keyword['path'].'">'.$keyword['keyword'].'</a>';
            }
            
            $return = implode(', ', $keywords);
        }
        
        return $return;
    }
}