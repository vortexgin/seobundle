<?php

namespace GinVorteX\SeoBundle\Manager;

class BaseManager{
    protected $_container;
    protected $_em;
    
    public function __construct() {
        global $kernel;

        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }
        
        $this->_container = $kernel->getContainer();
        $this->_em = $this->_container->get('doctrine')->getManager();
    }
    
    public function renderView($view, array $parameters = array()) {
        return $this->_container->get('templating')->renderResponse($view, $parameters)->getContent();
    }

}