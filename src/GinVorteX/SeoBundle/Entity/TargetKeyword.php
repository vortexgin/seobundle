<?php

namespace GinVorteX\SeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use GinVorteX\SeoBundle\Entity\Base as BaseEntity;

/**
 * TargetKeyword
 *
 * @ORM\Table(name="seo_keyword")
 * @ORM\Entity(repositoryClass="GinVorteX\SeoBundle\Repository\TargetKeywordRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TargetKeyword extends BaseEntity {

    /**
     * @var string
     *
     * @ORM\Column(name="keyword", type="string", length=255)
     */
    private $keyword;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * Set keyword
     *
     * @param string $keyword
     * @return TargetKeyword
     */
    public function setKeyword($keyword) {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string 
     */
    public function getKeyword() {
        return $this->keyword;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return TargetKeyword
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        return $this->path;
    }

}
