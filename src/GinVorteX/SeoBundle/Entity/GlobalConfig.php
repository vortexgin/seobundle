<?php

namespace GinVorteX\SeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use GinVorteX\SeoBundle\Entity\Base as BaseEntity;

/**
 * GlobalConfig
 *
 * @ORM\Table(name="seo_config")
 * @ORM\Entity(repositoryClass="GinVorteX\SeoBundle\Repository\GlobalConfigRepository")
 * @ORM\HasLifecycleCallbacks
 */
class GlobalConfig extends BaseEntity {

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255)
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="gplus", type="string", length=255, nullable=true)
     */
    private $gplus;

    /**
     * @var string
     *
     * @ORM\Column(name="defaultImage", type="string", length=255, nullable=true)
     */
    private $defaultImage;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255, nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="geoLocation", type="string", length=255, nullable=true)
     */
    private $geoLocation;

    /**
     * Set domain
     *
     * @param string $domain
     * @return GlobalConfig
     */
    public function setDomain($domain) {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain() {
        return $this->domain;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return GlobalConfig
     */
    public function setFacebook($facebook) {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string 
     */
    public function getFacebook() {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return GlobalConfig
     */
    public function setTwitter($twitter) {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string 
     */
    public function getTwitter() {
        return $this->twitter;
    }

    /**
     * Set gplus
     *
     * @param string $gplus
     * @return GlobalConfig
     */
    public function setGplus($gplus) {
        $this->gplus = $gplus;

        return $this;
    }

    /**
     * Get gplus
     *
     * @return string 
     */
    public function getGplus() {
        return $this->gplus;
    }

    /**
     * Set defaultImage
     *
     * @param string $defaultImage
     * @return GlobalConfig
     */
    public function setDefaultImage($defaultImage) {
        $this->defaultImage = $defaultImage;

        return $this;
    }

    /**
     * Get defaultImage
     *
     * @return string 
     */
    public function getDefaultImage() {
        return $this->defaultImage;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return GlobalConfig
     */
    public function setLanguage($language) {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return GlobalConfig
     */
    public function setCountry($country) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set geoLocation
     *
     * @param string $geoLocation
     * @return GlobalConfig
     */
    public function setGeoLocation($geoLocation) {
        $this->geoLocation = $geoLocation;

        return $this;
    }

    /**
     * Get geoLocation
     *
     * @return string 
     */
    public function getGeoLocation() {
        return $this->geoLocation;
    }

}
