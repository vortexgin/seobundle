<?php

namespace GinVorteX\SeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use GinVorteX\SeoBundle\Entity\Base as BaseEntity;

/**
 * MetaWeb
 *
 * @ORM\Table(name="seo_meta")
 * @ORM\Entity(repositoryClass="GinVorteX\SeoBundle\Repository\MetaWebRepository")
 * @ORM\HasLifecycleCallbacks
 */
class MetaWeb extends BaseEntity {

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=60, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=160, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="imagePage", type="string", length=255, nullable=true)
     */
    private $imagePage;

    /**
     * @var string
     *
     * @ORM\Column(name="altImage", type="string", length=255, nullable=true)
     */
    private $altImage;

    /**
     * @var string
     *
     * @ORM\Column(name="altTitle", type="string", length=255, nullable=true)
     */
    private $altTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="cloudText", type="text", nullable=true)
     */
    private $cloudText;

    /**
     * @var string
     *
     * @ORM\Column(name="cloudKeyword", type="text", nullable=true)
     */
    private $cloudKeyword;

    /**
     * Set path
     *
     * @param string $path
     * @return MetaWeb
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return MetaWeb
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MetaWeb
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return MetaWeb
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * Set imagePage
     *
     * @param string $imagePage
     * @return MetaWeb
     */
    public function setImagePage($imagePage) {
        $this->imagePage = $imagePage;

        return $this;
    }

    /**
     * Get imagePage
     *
     * @return string 
     */
    public function getImagePage() {
        return $this->imagePage;
    }

    /**
     * Set altImage
     *
     * @param string $altImage
     * @return MetaWeb
     */
    public function setAltImage($altImage) {
        $this->altImage = $altImage;

        return $this;
    }

    /**
     * Get altImage
     *
     * @return string 
     */
    public function getAltImage() {
        return $this->altImage;
    }

    /**
     * Set altTitle
     *
     * @param string $altTitle
     * @return MetaWeb
     */
    public function setAltTitle($altTitle) {
        $this->altTitle = $altTitle;

        return $this;
    }

    /**
     * Get altTitle
     *
     * @return string 
     */
    public function getAltTitle() {
        return $this->altTitle;
    }

    /**
     * Set cloudText
     *
     * @param string $cloudText
     * @return MetaWeb
     */
    public function setCloudText($cloudText) {
        $this->cloudText = $cloudText;

        return $this;
    }

    /**
     * Get cloudText
     *
     * @return string 
     */
    public function getCloudText() {
        return $this->cloudText;
    }

    /**
     * Set cloudKeyword
     *
     * @param string $cloudKeyword
     * @return MetaWeb
     */
    public function setCloudKeyword($cloudKeyword) {
        $this->cloudKeyword = $cloudKeyword;

        return $this;
    }

    /**
     * Get cloudKeyword
     *
     * @return string 
     */
    public function getCloudKeyword() {
        return $this->cloudKeyword;
    }

}
