<?php

namespace GinVorteX\SeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Base Entity
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class Base {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="postingDate", type="datetime")
     */
    private $postingDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updateDate", type="datetime")
     */
    private $updateDate;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set postingDate
     *
     * @param \DateTime $postingDate
     * @return TargetKeyword
     */
    public function setPostingDate($postingDate) {
        $this->postingDate = $postingDate;

        return $this;
    }

    /**
     * Get postingDate
     *
     * @return \DateTime 
     */
    public function getPostingDate() {
        return $this->postingDate;
    }

    /**
     * @ORM\PrePersist
     */
    public function setPostingDateValue() {
        $this->postingDate = new \DateTime();
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return TargetKeyword
     */
    public function setUpdateDate($updateDate) {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate() {
        return $this->updateDate;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setUpdateDateValue() {
        $this->updateDate = new \DateTime();
    }
}
