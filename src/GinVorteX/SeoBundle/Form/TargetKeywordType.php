<?php

namespace GinVorteX\SeoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TargetKeywordType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('keyword')
                ->add('path')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'GinVorteX\SeoBundle\Entity\TargetKeyword'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ginvortex_seobundle_targetkeyword';
    }

}
