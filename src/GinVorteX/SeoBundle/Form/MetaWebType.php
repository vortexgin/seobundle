<?php

namespace GinVorteX\SeoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use GinVorteX\SeoBundle\Manager\TargetKeywordManager;

class MetaWebType extends AbstractType{
    private $prefix_controller = 'SEOBundles_';

    private $manager;

    public function __construct() {
        $this->manager = new TargetKeywordManager();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        global $kernel;
        if($kernel instanceOf \AppCache) $kernel = $kernel->getKernel();
        $_container = $kernel->getContainer();
        
        $routes = [];
        foreach ($_container->get('router')->getRouteCollection()->all() as $name => $route){
            $route = $route->compile();            
            if(substr($name, 0, 1) != '_'){
                if(substr($name, 0, 11) != 'SEOBundles_'){
                    $_regex = $route->getRegex();
                    $_path_regex = substr($_regex, 2, strlen($_regex) - 5);
                    $routes[$name] = str_replace(array('(?P<', '>[^/]++)'), array('{', '}'), $_path_regex);                    
                }
            }
        }
        
        $selectKeyword = array();
        $listKeyword = $this->manager->getAll(array(), array('keyword' => 'ASC'));
        if ($listKeyword) {
            foreach ($listKeyword as $key => $obj) {
                $selectKeyword[$obj['id']] = $obj['keyword'] . ' => ' . $obj['path'];
            }
        }

        $builder
//                ->add('path')
                ->add('path', 'choice', array(
                        'choices' => $routes,
                        'required' => true,
                        'multiple' => false,
                        'attr' => array(
                            'style' => 'width:100%',
                        ),
                    )
                )
                ->add('title')
                ->add('description')
                ->add('keywords')
                ->add('imagePage')
                ->add('altImage')
                ->add('altTitle')
                ->add('cloudText')
                ->add('cloudKeyword', 'choice', array(
                        'choices' => $selectKeyword,
                        'required' => true,
                        'multiple' => true,
                        'attr' => array(
                            'style' => 'width:100%',
                        ),
                    )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'GinVorteX\SeoBundle\Entity\MetaWeb'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ginvortex_seobundle_metaweb';
    }

}
