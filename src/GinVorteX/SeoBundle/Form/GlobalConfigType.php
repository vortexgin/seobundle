<?php

namespace GinVorteX\SeoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GlobalConfigType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('domain')
                ->add('facebook')
                ->add('twitter')
                ->add('gplus')
                ->add('defaultImage')
                ->add('language')
                ->add('country')
                ->add('geoLocation')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'GinVorteX\SeoBundle\Entity\GlobalConfig'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ginvortex_seobundle_globalconfig';
    }

}
